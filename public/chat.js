// import { io } from "socket.io-client";


var username = prompt("Please tell me your name");
var role=prompt('role?')
var sessionId=Math.round(Math.random()*1000000)
var id = (document.getElementById("welcome").innerHTML += username);
var msg = document.getElementById("usermsg");
var output = document.getElementById("chatbox");


var reqUl=document.getElementById("roomreqs")

var butt = document.getElementById("submitmsg");
var talktoagentbutton = document.getElementById("talktoagent");
var joinasagentbutton = document.getElementById("joinasagent");
const userId=Math.round(Math.random()*1000000)
var agentJoinRoom=undefined

var socket = io.connect("ws://localhost:8080/livechat",{query: {
  userId: userId,
}});

socket.on("connection", (socket) => {
  console.log(socket);
});
socket.on("connect_error", (err) => {
  console.log(`connect_error due to ${err.message}`);
});


talktoagentbutton.addEventListener("click", () => {
  socket.emit('requestAgent', {userId,roomId:sessionId } );

});
joinasagentbutton.addEventListener("click", () => {
  socket.emit('joinAsAgent', { roomId:agentJoinRoom }  );

});
if(role=='agents'){
  socket.emit("joinRoom",{userId:username,roomId:'agents'})
  
  socket.on('agentRequested',(data)=>{
    console.log(data)
    reqUl.innerHTML +=
    `<li class='req_li' onclick='changeRoom(${data.roomId})'>` + data.userId + "</li>";
  msg.value = "";
  })
  butt.addEventListener("click", () => {
  socket.emit("message", {
    message: msg.value,
    username,
    timestamp: "12-13-99",
    roomId: agentJoinRoom,
  });
});
$('#joinrooms').css('display','flex')
 
}else{
  socket.emit("joinRoom", {userId:username,roomId:sessionId}, (err) => {
    if (err) {
      console.log(err.message, err);
    }
  });
  butt.addEventListener("click", () => {
  socket.emit("message", {
    message: msg.value,
    username,
    timestamp: "12-13-99",
    roomId: sessionId,
  });
});
}
socket.on('agentJoined',(data)=>{
  output.innerHTML +=
  "<p style='text-align:center'>" + 'Agent has joined the chat' + "</p>";
msg.value = "";
})
socket.on("message", function (data) {
  console.log(data);
  output.innerHTML +=
    "<p><strong>" + data.username + ":</strong>" + data.message + "</p>";
  msg.value = "";
});

socket.on("livechat-msg", function (data) {
  console.log(data);
  output.innerHTML +=
    "<p><strong>" + data.handle + ":</strong>" + data.message + "</p>";
  msg.value = "";
});


function changeRoom(roomId){
  agentJoinRoom=roomId
}